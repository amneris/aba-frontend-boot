# aba-frontend-boot
ABA frontend boot is an npm package that facilitates the building and running of aba's frontend applications. 

This library offers the following functionality:

## Remote app configuration

A web application's config (api endpoints, google tag manager sdk keys, etc.) varies between projects and deploys (qa, production, developer environments, etc.).
In order to seperate config from code, we recommend building the app's config at compile-time based on variables configured on a remote server. This server is an instance of
Spring Cloud Config. Spring Cloud Config provides server and client-side support for externalized configuration in a
distributed system. With the Config Server you have a central place to manage external properties for applications across all environments.

The process for integrating remote config with your web app is as follows:
 
 ```
 npm install aba-frontend-boot --save
 ```

An application's config must be downloaded before the application is either built or started. The command to do this is:
 
  
 ```
 node ./node_modules/aba-frontend-boot/bin/aba-frontend-boot.js downloadConfigThenRunNpmScript [application-name] [npm-script]
 ```
 
    
 __COMMAND ARGUMENTS:__ The above command takes the following arguments:
 
 * application-name : name of spring-cloud-config application containing your application's configuration
 * npm-script : name of script in package.json to execute after remote config has been downloaded



The application environment (development, production, etc.) and url of the Spring Cloud Config Server must be specified in environment variables named ENVIRONMENT and CONFIG_SERVER_URI. You can either manually set these environment variables, or 
alternativly you can create a .env file in the root of your project that sets these variables. An example of such a .env file is as follows:

```
ENVIRONMENT=minikube
CONFIG_SERVER_URI=http://cloud-config-server.aba.solutions
```

Finally, once the above have been executed, the variables are downloaded and stored in the root of your project in a file named config.json.

