var callModule = require('./downloadConfig');

var args = process.argv.slice(2);
var scriptToRun = args[1];

console.log('npm script to run after downloading config : ' + scriptToRun);

// downloads config from remote url and then runs the npm script passed as argument found in executing folder's package.json

callModule.downloadConfig(function () {
  require('child_process').execSync('npm run ' + scriptToRun, {stdio:[0,1,2]});
});
