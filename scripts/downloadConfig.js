var promisifiedHttp = require('./promisifiedHttp');
var fs = require('fs');
var environment = process.env.ENVIRONMENT || 'minikube';
var configServerEndpointRoot = process.env.CONFIG_SERVER_URI || 'cloud-config-server.aba.solutions';

var args = process.argv.slice(2);
var projectName = args[0];
var configServerPath = '/' + projectName + '/' + environment;

module.exports = {

  /**
   * downloads env variables from config server and stores them in a file named .env.json
   * where the script was executed from
   * @param callback
   */
  downloadConfig: function (callback) {

    // remove protocol component of url
    configServerEndpointRoot = configServerEndpointRoot.replace('https://', '');
    configServerEndpointRoot = configServerEndpointRoot.replace('http://', '');

    // create request paramenters
    var serverDetails = {
      host: configServerEndpointRoot,
      port: 80,
      path: configServerPath,
      method: 'GET'
    };

    console.log('DOWNLOADING ENV VARS FROM ' + configServerEndpointRoot + configServerPath);

    // get remote config
    promisifiedHttp.getData(serverDetails).then(function (response) {

      //parse response to get config info
      var config = JSON.stringify(response.propertySources[0].source);

      // write file to disk
      fs.writeFileSync('config.json', config);
      console.log(config);
      callback();

    }).catch(function (err) {
      console.log('ERROR DOWNLOADING ENV VARS : ' + err);
    });
  }
};
